# 1.Docker安装

1. 启动容器

```
docker run -itd --name redis-test -p 6379:6379 redis --bind 0.0.0.0
# 带密码
docker run -d --name redis-test -p 6379:6379 redis --requirepass "shengjie"
```
2. 生产级启动

```
docker run -d --privileged=true -p 6380:6379 
-v /docker/redis/conf/redis.conf:/etc/redis/redis.conf 
-v /docker/redis/data:/data 
--name redistest2 
redis:4.0 
redis-server /etc/redis/redis.conf --appendonly yes

```

```

参数说明：
--privileged=true：容器内的root拥有真正root权限，否则容器内root只是外部普通用户权限
-v /docker/redis/conf/redis.conf:  /etc/redis/redis.conf：映射配置文件
-v /docker/redis/data:/data：映射数据目录
redis-server /etc/redis/redis.conf：指定配置文件启动redis-server进程
--appendonly yes：开启数据持久化
 --bind 0.0.0.0 监听所有ip.
```

- 其他方法

```
Usage: ./redis-server [/path/to/redis.conf] [options]
       ./redis-server - (read config from stdin)
       ./redis-server -v or --version
       ./redis-server -h or --help
       ./redis-server --test-memory <megabytes>

Examples:
       ./redis-server (run the server with default conf)
       ./redis-server /etc/redis/6379.conf
       ./redis-server --port 7777
       ./redis-server --port 7777 --replicaof 127.0.0.1 8888
       ./redis-server /etc/myredis.conf --loglevel verbose

```



1. 连接redis  执行`redis-cli`
```
docker exec -it redis-test /bin/bash
```

## 1.1  设置数据

- 字符串  

    `set key "asd"`

    `set key "asd" nx` # 不要替换

    `set key "asd" xx` # 可以替换

    `get key`

- hashap

    `HMSET NAME field1 value1 field2 value2`

    `HGET NAME field2`

- 列表

    `lpush name item1 item2 item3`

    `lrange name start end`

- 集合set

    `sadd name set1 set2`

    `smembers name`

- zset(有序集合)

    `zadd key score member`

    `zrangebyscore name start end`

- 删除

	`del name`
	
- 获取所有的key 
	`keys *`
	
	

## 1.2  redis 命令行

redis-cli 连接本地数据库 ```redis-cli ```

```
redis-cli -h host -p port -a password
```

## 1.3 各种命令

| 序号       | 命令及描述                                             |
| :-----: | :----------------------------------------------------------- |
|  1   | [DEL key](https://www.runoob.com/redis/keys-del.html) 该命令用于在 key 存在时删除 key。 |
|  2   | [DUMP key](https://www.runoob.com/redis/keys-dump.html)  序列化给定 key ，并返回被序列化的值。 |
|  3   | [EXISTS key](https://www.runoob.com/redis/keys-exists.html)  检查给定 key 是否存在。 |
|  4   | [EXPIRE key](https://www.runoob.com/redis/keys-expire.html) seconds 为给定 key 设置过期时间，以秒计。 |
|  5   | [EXPIREAT key timestamp](https://www.runoob.com/redis/keys-expireat.html)  EXPIREAT 的作用和 EXPIRE 类似，都用于为 key 设置过期时间。 不同在于 EXPIREAT 命令接受的时间参数是 UNIX 时间戳(unix timestamp)。 |
|  6   | [PEXPIRE key milliseconds](https://www.runoob.com/redis/keys-pexpire.html)  设置 key 的过期时间以毫秒计。 |
|  7   | [PEXPIREAT key milliseconds-timestamp](https://www.runoob.com/redis/keys-pexpireat.html)  设置 key 过期时间的时间戳(unix timestamp) 以毫秒计 |
|  8   | [KEYS pattern](https://www.runoob.com/redis/keys-keys.html)  查找所有符合给定模式( pattern)的 key 。 |
|  9   | [MOVE key db](https://www.runoob.com/redis/keys-move.html)  将当前数据库的 key 移动到给定的数据库 db 当中。 |
|  10  | [PERSIST key](https://www.runoob.com/redis/keys-persist.html)  移除 key 的过期时间，key 将持久保持。 |
|  11  | [PTTL key](https://www.runoob.com/redis/keys-pttl.html)  以毫秒为单位返回 key 的剩余的过期时间。 |
|  12  | [TTL key](https://www.runoob.com/redis/keys-ttl.html)  以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)。 |
|  13  | [RANDOMKEY](https://www.runoob.com/redis/keys-randomkey.html)  从当前数据库中随机返回一个 key 。 |
|  14  | [RENAME key newkey](https://www.runoob.com/redis/keys-rename.html)  修改 key 的名称 |
|  15  | [RENAMENX key newkey](https://www.runoob.com/redis/keys-renamenx.html)  仅当 newkey 不存在时，将 key 改名为 newkey 。 |
|  16  | [SCAN cursor [MATCH pattern\] [COUNT count]](https://www.runoob.com/redis/keys-scan.html)  迭代数据库中的数据库键。 |
|  17  | [TYPE key](https://www.runoob.com/redis/keys-type.html)  返回 key 所储存的值的类型。 |

## 1.4 数据库
- 选择数据库 默认有十六个数据库 `select 3`
- 显示数据库数据大小 `dbsize`
- 清空所有数据库 `flushall`
- 清空当前数据库`flushdb`



# 2.权限设置

# 3.benckmark 性能测试服务

```Usage: redis-benchmark [-h <host>] [-p <port>] [-c <clients>] [-n <requests]> [-k <boolean>]

 -h <hostname>      Server hostname (default 127.0.0.1)
 -p <port>          Server port (default 6379)
 -s <socket>        Server socket (overrides host and port)
 -c <clients>       Number of parallel connections (default 50)
 -n <requests>      Total number of requests (default 10000)
 -d <size>          Data size of SET/GET value in bytes (default 2)
 -k <boolean>       1=keep alive 0=reconnect (default 1)
 -r <keyspacelen>   Use random keys for SET/GET/INCR, random values for SADD
  Using this option the benchmark will get/set keys
  in the form mykey_rand:000000012456 instead of constant
  keys, the <keyspacelen> argument determines the max
  number of values for the random number. For instance
  if set to 10 only rand:000000000000 - rand:000000000009
  range will be allowed.
 -P <numreq>        Pipeline <numreq> requests. Default 1 (no pipeline).
 -q                 Quiet. Just show query/sec values
 --csv              Output in CSV format
 -l                 Loop. Run the tests forever
 -t <tests>         Only run the comma-separated list of tests. The test
                    names are the same as the ones produced as output.
 -I                 Idle mode. Just open N idle connections and wait.```
```



#  4.集群

## 4.1 主从备份

- master

```
docker run -d --privileged=true --net=host \
-v /home/root/redis-jq/conf/redis.conf:/etc/redis/redis.conf \
--name master redis \
redis-server /etc/redis/redis.conf --port 6380


# config
#port 6380
requirepass shengjie
bind 0.0.0.0
daemonize no
save 900 1
#masterauth shengjie
#appendonly yes
replica-priority 1


```

- slave

```
docker run -d --privileged=true --net=host \
-v /home/root/redis-jq/conf/slave.conf:/etc/redis/redis.conf  redis \
redis-server /etc/redis/redis.conf --port 6383



# config
#port 6381
slaveof 127.0.0.1 6380
requirepass shengjie
bind 0.0.0.0
daemonize no
#save 900 1
masterauth shengjie
#appendonly yes

## 或者登陆成功之后执行
replicaof id port
# 查询集群消息
info replication
取消赋值
replicaof no one 
```

- 其他命令

```
# 查看角色
role
## 或者登陆成功之后执行
replicaof id port
# 查询集群消息
info replication
取消赋值
replicaof no one 
# 从服务器可写
--replica-read-only no/yes
config set replica-read-only no
# 当不一致的时候怎么办呢？
#  当做主服务的优先级，越小有有限。
replica-priority 1
```

## 4.2 redis sentinel

可以在主从备份中将备份角色转换为主角色。

```
docker run -d --privileged=true --net=host \
-v /home/root/redis-jq/conf/sentinel.conf:/etc/redis/redis.conf  redis \
redis-sentinel /etc/redis/redis.conf --port 6378

```
-  配置文件
```
sentinel monitor master1 127.0.0.1 6380 1
port 16380
sentinel auth-pass master1 shengjie
```

- 操作

```
# 指定端口通过redis-cli 登陆
# 列出所有masters
sentinel masters
# 列出name
sentinel master name 
# 获取slaves
sentinel  slaves master-naem
```



## 4.3 集群cluster

- 修改配置文件

```markdown
# 配置文件添加
cluster-enalbed yes

```

- 启动多个redis服务

```markdown
# docker 启动五个主master，分别启动多个sentinel进行监控。
# 实际上用sentinel已经没用了。
# 端口号从30001-30005
```

- 创建集群

```markdown
# 可以连接到任何一个master然后手动添加集群节点
cluster meet ip port
# 查看节点
cluster nodes
```

​			redis-cli --cluster是Redis客户端附带的集群管理工具，它的create子命令接受任意多个节点的IP地址和端口号作为参数，然后使用这些节点组建起一个Redis集群。create子命令允许使用多个可选参数，其中可选参数cluster-replicas用于指定集群中每个主节点的从节点数量。在上面的命令调用中，该参数的值为1，这表示我们想要为每个主节点设置一个从节点。

*  至少三个主，如果一主一从那就需要有六个服务。

```markdown
redis-cli --cluster  create 127.0.0.1:30001  127.0.0.1:30002 127.0.0.1:30003 \
127.0.0.1:30004 127.0.0.1:30005 127.0.0.1:30006  --cluster-replicas 1 -a shengjie

```



- 连接集群

```
redis-cli -c -p 30001
```

# 5.桌面

```
链接：https://pan.baidu.com/s/100B7Fkihzfm7TKQq14Aefg 
提取码：772q 
```

