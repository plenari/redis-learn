## 1.String

- `exists key` 某一个key是否存在。

- `move key 1` 删除zhegekey

- `expire name seconds` 设置过期时间

- `ttl key` 查询key还有多长时间过期

- `type key` 查看key的类型

- `append name value` 给字符串类型添加内容，如果key不存在就set 

- `strlen key` 长度

- `incr key` 加一个| `incrby key step`指定步长

- `decr key` 减少一个|`decrby key step`指定步长

- `getrange key start end` 截取字符串 -1 就是不限制

- `setrange key offset value` 相当于replace替换

- `setex key value seconds` 如果存在设置过期时间

- `setnx` 如果不存在就设置

- `mset k1 v1 k2 v2`可以设置多个

- `mget k1 k2` 可以设置多个

- `msetnx k1 v1 k2 v2 `设置多个值，只要都不存在才可以成功。

- `getset key vale`先获取在设置

- `cas`  
## 2.List

所有list 都是l开头的

- `lpush name v1 v2 v3` 将多个数值插入列表头部

- `lrang name end start`

- `rpush name v2 v3` 将数值插入尾部

- `rpop` 把队列右侧删掉

- `lpop` 

- `lindex name index` 获取数字第index

- `llen name` 获取数组长度

- `lrem name count key` 删除几个key，可是顺序是··怎么确定的呢？

- `ltrim name start end` 截取指定list 长度

- `rpoplpush source destination ` 从source 删除右侧一个元素，移动到新列表的左侧。

- `lset name index value` 给list指定位置设置值

- `linset name before value1 value` 给list insert 一个值

- 

  ​    

## 3. set

- `sadd name v1` 添加一个值
- `smembers name` 获取元素
- `sismember name v `是不是成员
- `scard name ` 中有多少个元素
- `srem name v` 删除set中元素
- `srandmember name `随机获取一个元素
- `srandmember name n `随机获取n个元素
- `spop k` 随机删除一些元素
- `sdiff name1 name2` 差集
- `sinter k1 k2` 交际
- `sunion k1 k2` 并集

## 4. Hash

- `hset name k1 v2  `
- `hget name k1`
- `hmset name k1 v1 k2 v2`
- `hmget name k1 k2`
- `hgetall name`
- `hdel name k1`
- `hlen name`
- `hexists name k1`
- `hkeys name`
- `hvals name`
- `incr name k` 给这个字段增加一个
- `hsetnx name  k v` 如果不存在则设置
- `hincrby name k num`

## 4.Zset

- `zdd k score value`  带排序的set
- `zrange k start end` 通过坐标获取元素
- `zrevrange k start stop`
- `zrangebyscore k -inf +inf` 通过分数负无穷正无穷，前后都包括
- `zrevrangebyscore k max min `通过分数反向获取
- `zrem name k`移除一个元素 
- `zcard name` 获取集合个数
- `zcount name min max`
- 



